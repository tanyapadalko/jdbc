import java.sql.*;
import java.time.LocalDateTime;
import java.util.Random;

public class MySQLConnection {
    private static final String DB_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DB_CONNECTION = "jdbc:mysql://localhost:3306/test1?useSSL=false";
    private static final String DB_USER = "user1";
    private static final String DB_PASSWORD = "12345678";

    private Connection dbConnection = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;

    private String selectQuery = "SELECT * FROM test1.user";
    private String updateQuery = "UPDATE test1.user SET name = ?, modifiedAt = ? WHERE id = ? ";

    private String[] NEW_NICKNAMES = new String[]{"TommenTheFlyingKing","BobbyBurger","BloodyMarry","TyrionTheMountain","JaimeTheHook"};


    void readDataBase() throws Exception {
        try {
            dbConnection = getDBConnection();
            dbConnection.setAutoCommit(false);

            statement = dbConnection.createStatement();
            resultSet = statement.executeQuery(selectQuery);
            System.out.println("READ FROM TABLE: ");
            writeResultSet(resultSet);


            preparedStatement = dbConnection.prepareStatement(updateQuery);
            for (int i = 0; i < NEW_NICKNAMES.length; i++) {
                preparedStatement.setString(1, NEW_NICKNAMES[i]);
                preparedStatement.setString(2, String.valueOf(LocalDateTime.now()));
                preparedStatement.setString(3, String.valueOf(i+1));
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();


            int sleepTime = getAmountOfMilisToSleep();
            System.out.println("THREAD sleep for : " + sleepTime);
            Thread.sleep(sleepTime);

            dbConnection.commit();
            System.out.println("Successful commit!");

        } catch (SQLException e) {
            System.out.println("Commit failed!");
            System.out.println(e.getMessage());
            dbConnection.rollback();
        } finally {
            close();
        }
    }


    private void writeResultSet(ResultSet resultSet) throws SQLException {
        while (resultSet.next()) {
            String id = resultSet.getString("id");
            String email = resultSet.getString("email");
            String name = resultSet.getString("name");

            System.out.println("Id: " + id);
            System.out.println("Email: " + email);
            System.out.println("Name: " + name);
        }
    }


    private Connection getDBConnection() {
        Connection dbConnection = null;
        try {
            Class.forName(DB_DRIVER);
            dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
            return dbConnection;
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            close();
        }
        return dbConnection;
    }

    private void close() {
        try {
            if (resultSet != null) resultSet.close();
            if (statement != null) statement.close();
            if (dbConnection != null) dbConnection.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private int getAmountOfMilisToSleep() {
        Random random = new Random();
        int lowerBound = 100;
        int upperBound = 1000;
        return random.nextInt(upperBound - lowerBound) + lowerBound;
    }

}
